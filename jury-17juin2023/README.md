# Jury final 17 juin 2023 de 9h à 13h

## Présentations PDF et abstracts graphiques

Déposez votre présentation pdf (16 slides) dans ce dossier et intulez-le de la manière suivante : "group-00.pdf" (en adaptant l'intitulé à votre numéro de groupe).

Déposez également votre abstract graphique (1 image PNG) et intulez-le de la manière suivante : "group-00.png" (en adaptant l'intitulé à votre numéro de groupe).

Voir détails des instructions [ici](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/final-projects/final-projects/#preparation-for-the-final-presentation)
