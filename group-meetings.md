# Group meetings

## Préparation au jury final

Voici quelques plages horaires ouvertes si vous désirez faire le point sur votre projet avec moi avant le jury final. Inscrivez votre groupe ci-dessous.

### Jeudi 8/6

13h30 à 14h00 :  
14h00 à 14h30 :  
14h30 à 15h00 :   
15h00 à 15h30 :   

### Jeudi 15/6

10h30 à 11h00 : [Mushroom X](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-05/)  
11h00 à 11h30 :         
11h30 à 12h00 : [A.L.E.D.](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-06/)     
12h00 à 12h30 :  

14h00 à 14h30 :  
14h30 à 15h00 :   
15h00 à 15h30 :  

## Préparation au préjury

Je souhaiterais **voir chaque groupe au moins une fois**, le **jeudi 27/4** et/ou le **jeudi 4/5**. J'ai établi un horaire ci-dessous dans lequel vous pouvez réserver votre créneau en indiquant le nom de votre groupe et le lien vers votre site web de groupe.  
Nous aborderons deux points : **vos prototypes de solution** et **votre dynamique/organisation d'équipe**.

exemple :  
10h30 à 10h50 : [Nom du groupe](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-template/)  

### Jeudi 27/4

10h30 à 10h50 :  
10h50 à 11h10 :  
11h10 à 11h30 : [Les ballers](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-10)      
11h30 à 11h50 : [El Nueve](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-9) 

14h00 à 14h20 :  
14h20 à 14h40 : [E-waste](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-01)    
14h40 à 15h00 :   
15h00 à 15h20 :   
15h20 à 15h40 :  
15h40 à 16h00 :  

### Jeudi 4/5

10h30 à 11h00 : [L'équipe en carton](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-03)  
11h00 à 11h30 : [Les héros de la Senne](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-02)     
11h30 à 12h00 :  
12h00 à 12h30 : [Mushroom X](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-05)

14h00 à 14h30 :  [Fabulous7](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-07/)  
14h30 à 15h00 :  [Dembe](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-04)    
15h00 à 15h30 :  [ALED](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-06)  
15h30 à 16h00 :  [EcoFlex](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-08)    
